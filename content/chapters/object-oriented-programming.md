---
title: Object Oriented Programming
---

Classes are the basis of object oriented programming in Raku and are one of
the basic organizational units in a Raku program as well.

# Object Oriented Programming

Classes are the basis of object oriented programming in Raku and are one of
the basic organizational units in a Raku program.

# The Mechanics of Class Definitions

The `class` keyword is used to define a new type, which has a set of attributes,
variables and methods, that are associated with and shared by a collection of
instances of such a class. The definition of a simple `BankAccount` class
is shown down below:

```perl
class BankAccount {
    has Str $.name;
    has Rat $.balance;

    # class variable to keep track of accounts created so far.
    my Int $.number-accounts = 0;

    method TWEAK() {
        $.number-accounts += 1;
    }

    #| Construct a bank account from the positional
    #| argument name and balance.
    multi method new( Str $name, Rat $balance ) {
        self.bless(name => $name, balance => $balance)
    }

    #| Deposit some amount into the account.
    method deposit( Rat $amount ) {
        $!balance += $amount
    }

    #| Withdraw some amount of money from account. Throw
    #| error when amount exceeds account's balance.
    method withdraw( Rat $amount ) {
        die 'Insufficient funds.' if $amount > $!balance;
        $!balance -= $amount
    }

    #| Decrement number of accounts by 1.
    method delete-account {
        $.number-accounts -= 1;
    }

    # String representation of the object.
    method Str {
        "BankAccount($!name, $!balance)"
    }

    #| Human representation of the object.
    method gist {
        "Account holder: $!name\nBalance: $!balance"
    }

    #| Evaluable representation of the object.
    method raku {
        self.^name ~ ".new(name => $!name, balance => {$!balance.raku})"
    }
}
```

Class definitions introduce class objects, instance objects and method objects.

## Class Objects

The execution of a class statement creates a class object. At the start of the
execution of a class statement, a new namespace is created and this serves as
the namespace into which all class attributes reside; **unlike languages like
Java, this namespace does not create a new local scope that can be used by class
methods hence the need for fully qualified names when accessing attributes.**

At the end of the execution of a class statement, a class object is created;
the scope preceding the class definition is reinstated, and the class object is
bound in this scope to the class name given in the class definition header. 

However, if the class created is an object then *what is the class of the class
object?*  Raku is built on a metaobject layer which means that there are objects
(i.e., the *metaobjects*) that control how various object-oriented constructs,
such as classes, behave. In order to obtain the metaobject behind an object,
you call the method `HOW` (Higher Order Workings) either on the class or 
its instance object.

```perl
> BankAccount.HOW
Perl6::Metamodel::ClassHOW.new
```

To get a better understanding of the metaobject for a class, we go behind the
scenes to explain what really goes on during the execution of a `class`
statement using the `BankAccount` example from above. For logistic reasons,
we won't reconstruct the entire class with all its methods.

```perl

```

An object can be instantiated from a class by calling the default `new`
constructor on the class, passing the optional named parameters to it. 
Methods are called on the object using the standard dot syntax; 
an object followed by dot and then attribute name: `obj.name`. 
Valid method calls are all declared methods and those methods that are
automatically created for you via the `$.` twigil. For example:

```raku
> my $x = BankAccount.new(name => 'Judith', balance => 250.0)
BankAccount.new(name => 'Judith', balance => 250)
> $x.name
Judith
> $x.inquiry()
10
```

Object instantiation is carried out by calling the default `new` constructor 
with the needed parameters. By default, the `new` constructor only accepts
named parameters which are named after the attributes declared with the `$.`
twigil. However, you can overload the `new` constructor or create your own
constructor if you wish. 


```raku
> my $x = BankAccount.new(name => 'Judith', balance => 10)
BankAccount.new(name => 'Judith', balance => 10)
```

## Instance Objects

If class objects are the cookie cutters then instance objects are the cookies
that are the result of instantiating class objects. Instance objects are
returned after the correct initialization of a class just as shown in the
previous section. Attribute references are the only operations that are valid on
instance objects. Instance attributes are either data attribute, better known as
instance variables in languages like Java, or method attributes.

## Method Objects

If `$x` is an instance of the `BankAccount class`, `$x.deposit`
is an example of a method object. Method objects are similar to functions,
however during a method's definition, an implicit argument is passed to
the method, the `self` argument.

# Customizing user-defined types

## The object creation process

# Inheritance

Inheritance is one of the basic tenets of object oriented programming and
Raku supports multiple inheritance like programming languages such as C++
and Python. Inheritance provides a mechanism for creating new classes that
specialise or modify a base class thereby introducing new functionality.
We call the base class the *parent class*. An example of a class inheriting
from a base class in python is given in the following example.

Classes implement inheritance via the `is` trait which accepts a type object
to be added as a parent class of a class in its definition (i.e.,
`class Child is Parent`).

```perl
class BankAccount {
    has Str $.name;
    has Rat $.balance;

    # class variable to keep track of accounts created so far.
    my Int $.number-accounts = 0;

    method TWEAK() {
        $.number-accounts += 1;
    }

    #| Construct a bank account from the positional
    #| argument name and balance.
    multi method new( Str $name, Rat $balance ) {
        self.bless(name => $name, balance => $balance)
    }

    #| Deposit some amount into the account.
    method deposit( Rat $amount ) {
        $!balance += $amount
    }

    #| Withdraw some amount of money from account. Throw
    #| error when amount exceeds account's balance.
    method withdraw( Rat $amount ) {
        die 'Insufficient funds.' if $amount > $!balance;
        $!balance -= $amount
    }

    #| Decrement number of accounts by 1.
    method delete-account {
        $.number-accounts -= 1;
    }

    # String representation of the object.
    method Str {
        "BankAccount($!name, $!balance)"
    }

    #| Human representation of the object.
    method gist {
        "Account holder: $!name\nBalance: $!balance"
    }

    #| Evaluable representation of the object.
    method raku {
        self.^name ~ ".new(name => $!name, balance => {$!balance.raku})"
    }
}

class SavingsAccount is BankAccount {
	has $.rate;
	
    #| Evaluable representation of the object.
    method raku {
        self.^name ~ ".new(name => $!name, balance => {$!balance.raku})"
    }
}
```

## Multiple inheritance

In multiple inheritance, a class can have multiple parent classes. This type of hierarchy is strongly discouraged. One of the issues with this kind of inheritance is the complexity involved in properly resolving methods when called. Imagine a class, D, that inherits from two classes, B and C and there is a need to call a method from the parent classes however both parent classes implement the same method. How is the order in which classes are searched for the method determined ? A Method Resolution Order algorithm determines how a method is found in a class or any of the class’ base classes. In Python, the resolution order is calculated at class definition time and stored in the class __- dict__ as the __mro__ attribute. To illustrate this, imagine a class hierarchy with multiple inheritance such as that showed in the following example.

# Composition

# Static and class methods

# Abstract classes

Oftentimes it is necessary to enforce a contract between classes in a program.
For example, it may be necessary for all classes to implement a set of
common methods to which all classes must respond. This is accomplished
using interfaces and abstract classes in statically typed languages like Java.
In Raku, we implement *abstract classes* using roles with stubbed methods which
are to be *mixed* into the derived classes. 

{{< hint info >}}

A **stub method** is a method whose implementation is deferred to other classes.

```
role Shape {
    method size { ... }  # the ... (yada-yada operator) indicates a stub 
}
```
{{< /hint >}}

Once a role with stubbed methods is mixed in, each subclass must provide their
own implementation for each of the stubbed methods.

```perl
role Vehicle {
	method change-gear { ... }
	method start-engine { ... }
}

class Car does Vehicle {
	has $.make;
	has $.model;
	has $.color;
}

```

Simply running the code in the example above will throw a compile-time error:

```perl
===SORRY!=== Error while compiling /home/runner/qx7mc4yvaua/./main.raku
Method 'change-gear' must be implemented by Car because it is required by roles: Vehicle.
```

Once, a class implements all stubbed methods the class becomes a concrete
class and can be instantiated by a user.

```perl
role Vehicle {
	method change-gear { ... }
	method start-engine { ... }
}

class Car does Vehicle {
	has $.make;
	has $.model;
	has $.color;
	
	method change-gear { say 'Changing gear' }
	
	method start-engine { say 'Changing engine' }
}
```

Instantiating the concrete class works as expected:

```perl
> my $car = Car.new(make => 'Toyota', model => 'Corolla', color => 'blue');
Car.new(make => 'Toyota', model => 'Corolla', color => 'blue')
> $car ~~ Vehicle
True
> $car.does(Vehicle) # same as earlier
True
>
```
