---
title:  A Short Tutorial
---

This short tutorial introduces the reader informally to the basic concepts and
features of the [Raku programming language](https://raku.org/). It is not
intended to be a comprehensive introductory tutorial to programming for a
complete novice but rather assumes the reader has had previous experience
programming with another programming language, especially an object oriented
one.

# The Rakudo Compiler

In this book, we'll be using the Rakudo compiler, which is an implementation
of the Raku programming language. To install it, visit the [Rakudo
website](https://rakudo.org/) and follows the installation instructions for your
respective operative system. 

Once Rakudo is installed, you can type `raku` in the command line and you should
be greeted with a Raku interactive session with the REPL. The primary prompt,
`>`, signals a user to enter statetements while the secondary prompt, `*`,
signals a continuation line as shown in the following example:

```perl
> sub hello() {
*    say "Hello World"
* }
>
```

A user can type in Raku statements at the interpreter prompt and get instant
feedback. For example, we can evaluate expressions at the REPL and get values
for such expressions as shown down below:

```perl
To exit type 'exit' or '^D'
> my $var-1 = 3
3
> my $var-2 = 2
2
> $var-1 * $var-2
6
```

As indicated by the first line, typing `exit` or `Ctrl-D` at the primary
prompt causes the interpreter to exit the session.

# Raku Statements

A program in Raku is composed of a number of logical lines and each of these
logical lines is delimited by a semicolon (`;`). Compound statements however can
be made up of multiple logical lines. In most places where spaces appear in a
statement, and before the semicolon, it may be split up over many lines. Also,
multiple statements may appear on the same line.

```perl
> say "Hello";
Hello
> say "World"
World
> say "Hello"; say "World"
Hello
World
>
```

Since statements are delimited by semicolons, in Raku there is not a distinction
between simple statements that spans a single logical line and compound
statements that spans multiple logical lines. As long as the statements are
separated by semicolons, multiple lines can span a single line.

```perl
> if 2 == 2 {
*     say '2 is equal to itself';
*     say 'This is another statement';
* }
2 is equal to itself
This is another statement
> if 2 == 2 { say '2 is equal to itself'; say 'This is another statement'; }
2 is equal to itself
This is another statement
>
```

# Strings

In their most simple form, strings are represented in Raku using double `"..."`
or single `'...'` quotes. Special characters can be used within a string by
escaping them with `\` as shown in the following example:

```perl
> my $name = 'men\'s'
men's
>
```

An important difference between single and double quotes is that the latter
allows for the interpolation of variables:

```perl
> my $name = 'John'
John
> "My name is $name"
My name is John
> 'My name is $name'
My name is $name
```

Strings are immutable so once created they cannot be modified. There is no
`character` (or `char`) type so characters are assumed to be strings of length 1.
Unlike in other programming languages, strings aren't sequence types so 
positional type operations aren't supported (i.e., indexing). However, there are
several methods that offer the same capabilities. For example the `substr`
routine:

```perl
> my $name = "book"
book
> $name.substr(0, 1) # same result as $name[1] in other languages
b
>
```

Strings can be concatenated with the infix operator `~` which coerces both
arguments to `Str` and concatenates them.

```perl
> 'Ra' ~ 'ku'
Raku
> 1 ~ 2
12
>
```

The prefix version of the operator `~` coerces the argument to `Str` by calling
the `Str` method on it.

```perl
> 1 ~~ Int
True
> ~1 ~~ Int
False
>
```

In the above example, `~~` is the smartmatch operator. In the former example,
`1 ~~ Int` returns `True` because `1` is an integer and thus matches against the
`Int` type. In the latter example, `~1 ~~ Int` returns `False` because `1`
is coerced into a string (`Str`) and thus its match against the `Int` type
returns `False` as expected. We'll discuss the smartmatch operator more in-depth
later on.

In order to get the length of a string, we use the built-in method
`chars`. This routine coerces the invocant/argument to `Str`, and returns the
number of characters in the string.

```perl
> "hello".chars
5
> chars "hello"
5
>
```

The `Str` type offers a wealth of both methods and subroutines, thus
you're encouraged to read its documentation at https://docs.raku.org/type/Str.

# Flow Control

## `if-else` and `if-elsif-else` Statements

Raku supports the `if` statement for conditional execution of a code block.
The code block attached to the condition will only be evaluated if the condition
returns `True` when coerced to `Bool` (Raku's Boolean type). Unlike in some 
languages the condition does not have to be parenthesized, instead the `{`
and `}` around the block are mandatory:

```perl
> my $name = 'John'
> if $name eq 'John' { say 'Hello, John!' }
Hello, John!
>
```

The `if` statement can be followed by zero or more `elsif` statements and an
optional `else` statement that is executed when none of the conditions in the
`if` or `elsif` statements have been met.

```perl
> my $name = 'Fuu'
Fuu
> if $name eq 'John' {
*     say 'Hello, John!'
* }
* elsif $name == 'Chuck'
*     say 'Hello, John!'
* }
* else {
*     say 'Hello, Stranger!'
* }
Hello, Stranger!
>
```

There is also a form of `if` called a *statement modifier* form, in which
the `if` statement and the condition come after the code you want to run
conditionally. Please note that the condition is still always evaluated first
and that although useful, the statement modifier form is probably best used
sparingly:

```perl
> my $name = 'Fuu'
Fuu
> say "My name's Fuu" if $name eq 'Fuu'
My name's Fuu
>
```

If you ever get tired typing *"if not (X)"* you may use the `unless` statement
to invert the sense of a conditional statement. To avoid confusion, you cannot
use `else` or `elsif` with `unless`. Aside from those two differences, `unless`
works the same as `if`:

```perl
> my $name = 'Mugen'
Mugen
> unless $name eq 'Fuu' { say "You're not Fuu" }
You're not Fuu
> say "You're not Fuu" unless $name eq 'Fuu' 
You're not Fuu
>
```

## `with-orwith` and `with-orwith-else` Statements

Statements such as `if` tests for *truthiness* while statements such as `with`
tests for *defidness*. In the former, the condition is coerced into a Boolean
by the explicit call to `Bool` while in the latter the condition is tested
for its defidness by calling the `defined` routine on it. For example,
the value `0` has a truth value of `False`, however it's a defined value. As
demonstrated in the following example, choosing between `if` and `with`
makes a tremendous different:

```perl
> if "abc".index("a") { say 'Found an a' }
()
> with "abc".index("a") { say 'Found an a' }
Found an a
>
```

In the first case, `"abc".index("a")` finds an `a` whose index is `0` and
evaluates to `False` with `if` and the block isn't executed. In the second
case, `"abc".index("a")` finds an `a` whose index is `0`, a defined value,
thus the block is executed. Certainly we could've just called `defined` on 
`"abc".index("a")` so that `if` would've executed the block. However,
there's another thing that sets `with` apart: it topicalizes in the condition,
i.e., it stores the condition's value in the topic variable `$_`. For example:

```perl
> with "abc".index("a") { say 'Index: ', $_ }
Index: 0
>
```

The `with` statement can be followed by zero or more `orwith` statements and an
optional `else` statement that is executed when none of the conditions in the
`with` or `orwith` statements have been met.

```perl
> my $s = "abc"
abc
> with $s.index("a") {
*     say "Found a at $_"
* }
* orwith $s.index("b") {
*     say "Found b at $_"
* }
* orwith $s.index("c") {
*     say "Found c at $_"
* }
else {
*     say "Didn't find a, b or c"
* }
Found a at 0
>
```

Similar to `if`, there's a statement modifier form for `with`.

```perl
> my $s = 'abc'
abc
> say "Found a at $_" with $s.index("a")
Found at at 0
>
```

If you ever get tired typing *"with not (X)"* you may use the `without` statement
to invert the sense of a conditional statement (i.e., it checks for
*undefinedness*). As with `unless`, you may not add an `else` clause.
`without` also has a statement modifier form.

```perl
> my $answer = Any;
(Any)
> without $answer { say "Got: {$_.raku}" }
Got: Any
> { say "Got: {$_.raku}" } without $answer
Got: Any
>
```

## `for` statements

The `while` and `for` statements constitute the main looping constructs provided
by Raku. 

The `for` statement in Raku is used to iterate over list-y types (`List`s, 
`Array`s, `Seq`s, etc.) More generally, the `for` loop is used to iterate over
any object that implements the `Iterable` role (i.e., in conjuntion with the
`Iterator` role, `Iterable` forms the Raku iterator protocol). This role serves
as an API for objects that can be iterated with `for` and related iteration
constructs, like assignment to a `Positional` variable. This will be discussed
further in chapters that follow.

The following snippet illustrates the usage of the `for` loop:

```perl
> my $names = ('Joe', 'John', 'Jin', 'Jaira')
(Joe John Jin Jaira)
> for $names -> $name { say $name }
Joe
John
Jin
Jaira
>
```

Most programming languages have a syntax similar to the following for iterating
over a progression of numbers:

```c
for (int x = 10; x < 20; x = x+1) {
    // do something here
}
```

The same can be done in Raku with the `loop` construct:

```perl
loop (my $x = 10; $x < 20; $x = $x+1) {
    # do something here
}
```

However, Raku offers neater ways to create progression of numbers/strings
via the range operator `..` (for the `Range` type). The two main purposes
of ranges in Raku are to generate lists of consecutive numbers or strings, and
to act as a matcher to check if a number or string is within a certain range.
Ranges are constructed using one of the four possible range operators:
`a..b` ({{< katex >}}a \leq x \leq b{{< /katex >}}),
`a^..b` ({{< katex >}}a < x \leq b{{< /katex >}}),
`a..^b` ({{< katex >}}a \leq x < b{{< /katex >}}), and
`a^..^b` ({{< katex >}}a < x < b{{< /katex >}}).

```perl
> for 10..12 -> $i { say $i }
10
11
12
for 'a'..'c' -> $l { say $l }
a
b
c
> 1 ~~ 10..15
False
>
```

Another way to create sequence of values is via the `...` operator (for the
`Seq` type). There are important differences between `Range`s and `Seq`uences
but for now we only highlight the most obvious one: A `Range` is restricted to
going small to larger elements; a `Seq` isn't. We'll talk more in-depth about
`Seq`s later on.

```perl
> for 10...12 -> $i { say $i }
10
11
12
for 'c'...'a' -> $l { say $l }
c
b
a
> 1 ~~ 10...15
False
>
```

## `while` Statement

The `while` statement executes the code block as long as the condition 
evaluates to `True`.

```perl
> my $counter = 5
5
> while $counter > 0 {
*    say $counter;
*    $counter = $counter - 1
* }
5
4
3
2
1
>
```

If you'd like to execution a code block as long as the condition evaluates to
`False`, then you can use the `until` statement.

```perl
> my $counter = 5
5
> until $counter == 0 {
*    say $counter;
*    $counter = $counter - 1
* }
5
4
3
2
1
>
```

## `given` statement

The `given` statement is Raku's topicalizing keyword. This is similar to the
`switch` keyword that topicalizes in languages such as C or the `case` statement
in Ruby. In other words, `given` sets the topic variable `$_` inside the block
that follows it. The keywords for individual cases are `when` and `default`.

```perl
my $capacity = 22;
given $capacity {
    when $_ ~~ 0 { # Explicit smartmatching againts $_
        say 'You ran out of gas.'
    }
    when 1..20 {   # Implicit smartmatching againts $_
        say 'The tank is almost empty. Quickly, find a gas station!'
    }
    when 21..70 {
        say 'You should be ok for now.'
    }
    when 71..100 {
        say 'The tank is almost full.'
    }
    default {
        say 'Error: capacity has an invalid value ($capacity)'
    }
    
    say 'This will never be said :(';
}
```

A block containing a `default` statement will be left immediately when
the block after the default statement is left. A `when` statement does
the same thing. As illustrated in the example above, the `when` statement
smartmatches the topic (`$_`) against a supplied expression; it is possible
to check against values, regular expressions, and types when specifying a match.

The `when` block is similar to an `if` block and either or both can be used 
in an outer block; they also both have a *statement modifier form*. 
However, there is a differece in how the code that precedes them in the block that
contains them is handled. On one hand, when the `when` block is executed, control
is passed to the enclosing block and following statements are ignored. For
the contrary, when the `if` block is executed, following statements are
executed. For example:

```perl
{
	my $number = 4;
	when $number > 0  { say 'greater than zero' }
	when $number %% 2 { say 'divisible by 2' }
    say 'This will never be said';
}

# OUTPUT:
# greater than zero

{
	my $number = 4;
	if $number > 0  { say 'greater than zero' }
	if $number %% 2 { say 'divisible by 2' }
    say 'This will be said';
}

# OUTPUT:
# greater than zero
# divisible by 2
# This will be said
```

{{< hint info >}}
Similar to `if`, `when` has a statement modifier form. However, unlike its
regular form, it does not affect execution of following statements either
inside or outside of another block:

```perl
{
	my $number = 4;
	say 'greater than zero' when $number > 0;
	say 'divisible by 2'    when $number %% 2;
    say 'This will be said';
}

# OUTPUT:
# greater than zero
# divisible by 2
# This will be said
```
{{< /hint >}}

In order to prevent a `when` or `default` statement to exit its outer block,
there's a `proceed` statement. This statement will immediately leave the `when`
or `default` block, skipping the rest of the statements, and resuming after the
block attempting to match the given value once more. For example:

```perl
{
	my $number = 4;
	when $number > 0  { say 'greater than zero'; proceed }
	when $number %% 2 { say 'divisible by 2';    proceed }
	say 'This will be said!' 
}

# OUTPUT:
# greater than zero
# divisible by 2
# This will be said!
```

By contrast, the `succeed` statement short-circuits execution and exits
the entire given block at that point. It may also take an argument to 
specify a final value for the block. For example:

```perl
{
	my $number = 4;
	when $number > 0  { 
        say 'greater than zero';
        succeed "Found it";
        say 'something about non-negative numbers'; # never said though 
    }
	when $number %% 2 { say 'divisible by 2';    proceed }
	
	say 'This will be said!' 
}

# OUTPUT:
# greater than zero
```

There are a few things to point out about `proceed` and `succeed`:

* They're meant to be used only inside `when` or `default` blocks. Thus, 
  it's an error to use them if you are not inside a `when` or `default` block.

* Any `proceed` or `succeed` in the statement modifier form of `when` applies
  to the surrounding clause.

## `last` and `continue` Statements

The `last` keyword is used to escape from an enclosing loop. Whenever the `last`
keyword is encountered during the execution of a loop, the loop is abruptly
exited and no other statement within the loop is executed.

```perl
> for 0..10 -> $i {
*     if $i == 5 { break }
*     else       { say $i }
* }
0
1
2
3
4
>
```

The `next` keyword is used to skip to the next iteration of a loop. When used
the interpreter ignores all statements that come after the continue
statement and continues with the next iteration of the loop.

```perl
> for 0..5 -> $i {
*     if $i == 2 { continue }
*     say "The value is $i"
* }
The value is 0
The value is 1
# no value printed for $i == 2
The value is 3
The value is 4
The value is 5
>
```

## `repeat`/`while` and `repeat`/`until` statements

These statement execute the block at least once and, if the condition
allows, repeats that execution. If `while` is used, then repeat as long as
the condition is `True`. If `until` is used, then repeat as long as the
condition is `False`.

```perl
> my $x = 3;
> repeat {
*     say $x;
*     $x--;
* } while $x > 0
3
2
1
> my $y = 0;
> repeat {
*     say $y;
*     $y++;
* } until $y == 3;
0
1
2
>
```

## Enumerations

Sometimes, when iterating over a list, a hash, etc., having access to both the
item and its index may be necessary. This could achieved using a `while` loop as
shown in the following example:

```perl
> $names = ('Joe', 'Garo', 'Trey', 'Leo')
(Joe Garo Trey Leo)
> my $name-count = $names.elems
4
> my $index = 0
> while $index < $name-count {
*     say "$index. {$names[$index]}"
*     $index += 1
* }
0. Joe
1. Garo
2. Trey
3. Alex
>
```

The above solution is how one would go about it in most programming languages
but Raku has a better alternative to this in the form of several related
routines:

* `keys` - Returns a sequence of indexes from the list.

```perl
> for $names.keys -> $index {
*     say "$index. {$names[$index]}"
* }
0. Joe
1. Garo
2. Trey
3. Leo
>
```

* `keys` - Returns a sequence of the list elements, in order.

```perl
> for $names.values -> $name {
*     say "$name"
* }
Joe
Garo
Trey
Leo
>
```

* `kv` - Returns an interleaved sequence of indexes and values.

```perl
> for $names.kv -> $index, $name {
*     say "$index. $name"
* }
0. Joe
1. Garo
2. Trey
3. Leo
>
```

* `pairs` - Returns a sequence of pairs, with the indexes as keys and the list
  values as values.

```perl
> for $names.pairs -> $pair {
*     say "{$pair.key}. {$pair.value}"
* }
0. Joe
1. Garo
2. Trey
3. Leo
>
```

* `antipairs` - Returns a sequence of pairs, with the values as keys and the
  indexes as values, i.e. the direct opposite to the `pairs` routine.

```perl
> for $names.antipairs -> $ap {
*     say "{$ap.value}. {$pair.key}"
* }
0. Joe
1. Garo
2. Trey
3. Leo
>
```

All these methods are not only available to `List`s but also to `Array`s,
`Map`s, `Hash`es, etc. What they return depends on the type they're called
on. For example, `keys` on a `Map` returns a sequence of all keys in it.

# Subroutines

Named subroutines are defined with the `sub` keyword which must be followed by
the subroutine name and the parenthesized list of parameters, which can be
either positional or named parameters, or both. The `return` keyword is used to
return a value from a subroutine definition. 

```perl
sub fullname( $firstname, $lastname ) {
    return "$firstname $lastname"
}
```

Subroutines are invoked by calling the function name with required arguments.
For example, `fullname "Ralph", "Delos"`. Parenthesis aren't needed unless to
disambiguate. For instance:

```perl
> fullname "Ralph", "Delos.uc
Ralph DELOS
> fullname("Ralph", "Delos).uc
RALPH DELOS
```

Raku subroutines can return multiple values by returning a list of the 
required values as shown in the example below in which we return the
quotient and remainder from a division operation:

```perl
> sub divide( Int $a, Int $b ) { return $a div $b, $a mod $b }
&divide
> divide 7, 2
(3 1)
```

Raku subroutines can be defined without `return` keyword, in which case the
value of the last expression is returned. In the following example,
the `print-name` subroutine ends up returning the value the `say` returns,
i.e., `True`:

```perl
> sub print-name( Str $firstname, Str $lastname ) {
*    say "$firstname $lastname"
* }
&print-name
> print-name 'Richard', 'Feynman'
Richard Feynman
> my $x = print-name 'Richard', 'Feynman'
Richard Feynman
> $x.^name
Bool
> $x
True
>
```

The `return` keyword does not even have to return a value in Raku as shown in
the following example:

```perl
> sub dont-return-value {
*     say "The return keyword without argument"
*     return
* }
&dont-return-value
> dont-return-value()
The return keyword without argument
>
```

In Raku, all blocks (even those associated with if, while, etc.) are *anonymous
functions*. A block that is not used as an rvalue is executed immediately.
Thus Raku has:

* Fully anonymous, which is called as created.

```perl
> { say 'I got called' }
I got called
>
```

* Assigned to a variable

```perl
> my $squarer1 = -> $x { $x * $x };             # pointy block
-> $x { #`(Block|94173564476184) ... }
> my $squarer2 = { $^x * $^x };                 # self-declared parameters
-> $x { #`(Block|94173564477120) ... }
> my $squarer3 = { my $x = shift @_; $x * $x }; # Perl 5 style
-> *@_ { #`(Block|94173579749752) ... }
>
```

* Currying

```perl
> sub add( $m, $n ) { $m + $n }
&add
> my $seven = add(3, 4);
7
> my $add-one = &add.assuming(1);
&__PRIMED_ANON
> my $eight = $add-one($seven);
8
>
```

* `WhateverCode` object 

```perl
> my $w = * - 1;       # WhateverCode object
{ ... }
> my $b = { $_ - 1 };  # same functionality, but as Callable block
```

* Using the `sub` keyword without a subroutine's name. Unless the returned
subroutine is stored in `&`-sigilled variable, you need parenthesis to perform
a call. 

```perl
> my $add = sub ($m, $n) { $m + $n }
sub { }
> $add(3, 4)
7
> my &add = sub ($m, $n) { $m + $n }
sub { }
> &add 3, 4
7
>
```

# Data Structures

Raku has a number of built-in data structures that make programming easy. The
built-in data structures include `List`s, `Array`s, `Map`s, and `Hash`es.

## `List`s

A `List` stores items sequentially and potentially lazily. Unlike other
languages, Raku uses the comma for creating lists, and thus parenthesis aren't
needed unless to create an empty list or nested lists. Lists can also be created
using the `list` routine (in either its function or method form). Alternatively,
you can use the `List` type to coerce its argument/invocant into a list.
The empty list is denoted by `()`. The `List` data structure implements the
`Positional` role and as such provides support for subscripts (e.g., integer
indexing). Lists are 0-indexed and go up to the number of elements in the
list minus one.

```perl
> my $names = ('ralph', 'orquidia', 'dana')
(ralph orquidia dana)
> $names[0]
ralph
>
```

`List`s are immutable objects, in other words, neither the number of elements in
a list nor the elements themselves can be changed. Thus it's not possible to use
operations (e.g., `shift`, `push`, etc.) that change the data structure itself.

```perl
> my $earth-moons = ('Moon',)
(Moon)
> $earth.push('Deira')
Cannot call 'push' on an immutable 'List'
  in block <unit> at <unknown file> line 1

> $earth-moons[0] = 'The Moon'
Cannot modify an immutable List ((ralph))
in block <unit> at <unknown file> line 1

>
```

A `List` doesn't place its elements in containers, which is the reason 
why they cannot be assigned to. Although neither a list nor its elements can be
mutated by default, if any element happens to be in a `Scalar` container, then
the element's contents can be replaced via an assignment operation. We'll go
over containers and their importance in Raku later on.

```perl
> my $moon = 'Moon'
Moon
> my $mars-moons = ($moon, 'Deimos')
(Moon Deimos) 
> $mars-moons[0] = 'Phobos'
Phobos
> $mars-moons
(Phobos Deimos)
>
```

If you're creating a list of strings, you can use the quote-words operator `<>`,
which breaks up the contents on whitespace and returns a `List` of the words.

```perl
> my $names = <Lidia Helvia Laura> # same as ('Lidia', 'Helvia', 'Laura')
(Lidi Helvia Laura)
> $names[0] ~ 'and' ~ $names[1]
Lidia and Helvia
>
```

## `Array`s

An `Array` is a `List` which forces all its elements to be `Scalar` containers,
which means you can assign to array elements. In addition, an `Array` is itself
mutable and you can alter it with operations such as `push`, `shift`, `pop`,
etc. Arrays are created using square brackets, `[]`, or with the `Array` type
by coercing its argument/invocant.

```perl
> my $mars-moons = ['Moon']
[Moon]
> $mars-moons[0] = 'Phobos'
Phobos
> $mars-moons.push('Deimos')
Deimos
> $mars-moons
[Phobos Deimos]
>
```

We've used only `$`-sigilled variables (also known as **scalar variables**) so
far. Raku also provides us with `@`-sigilled variables (also knowm as **positional
variables**) with the explicit purpose of storing only `Positional` data structures
such as `List`s, `Array`s, `Range`s, etc. The default type for a `@`-sigilled
variable is `Array`. Thus, when assigning a comma-separated list of things
to a `@`-sigilled variable, we can drop the array constructor operator `[]`;
we end up with an `Array` anyway.

```perl
> my @galilean-moons = ('Io')
[Io]
> @galilean-moons.push('Europa', 'Ganymede', 'Callisto')
[Io Europa Ganymede Callisto]
>
```

## `Map`s

A `Map` is an immutable mapping from string keys to values of arbitrary types.
It implements the `Associative` role which provides support for looking up
values using keys (i.e., associative subscripting). To create a `Map` object, we
must instantiate the class with a list of interleaved keys and values.

```perl
> my $ages = Map.new('Lia', 24, 'Hale', 20, 'Williams', Nil)
Map.new((Hale => 20, Lia => 24, Ron => 21))
> $ages{'Hale'}
20
>
```

To retrieve a value from the `Map` by key, use the `{ }` postcircumfix
operator as shown in the example below:

```perl
> $ages{'Hale'}
20
> $ages<Lia> # using the quote-words operator
24
>
```

To check whether a given key is stored in a `Map`, you can use the `:exists`
adverb:

```perl
> $ages<William>.defined
False
> $ages<William>:exists
True
> $ages<Lia>:exists
True
>
```

{{< hint info >}}
In Raku, **adverbs** are named parameters/arguments that modify a routine's
behavior and they work similar to how adverbs work in natural languages. They
are usually expressed with colon pair notation (i.e., adverbial pair form).

```perl
:true(1) # same as "true" => 1
```

In the above example, the `:exists` adverb modifies `$ages<Lia>` in that 
instead of returning the element's actual value, it returns whether or not the
requested element exists. This can be used to distinguish between elements with
an undefined value, and elements that aren't part of the collection at all.
{{< /hint >}}

Since `Map` is an immutable data structure, it is not possible to add keys 
after it has been initialized:

```perl
my $m = Map.new( 'a', 1, 'b', 2 );
$m{ 'c' } = 'foo'; # WRONG! 
                   # Cannot modify an immutable Str
```

{{< hint info >}}
The order in which keys, values and pairs are retrieved is generally
arbitrary, however the `keys`, `values` and `pairs` methods return them always
in the same order when called on the same object.
{{< /hint >}}

## `Hash`es

`Hash`es are to `Map`s what `Array`s are to `List`s. In other words, a `Hash` is
just a mutable `Map`. Thus, the primary operations offered by `Hash`es are both
the storage of a value by a key and its retrieval by the same key.

```perl
> my $ages = %() # empty hash
> $ages{'Lia'} = 24
24
> $ages{'Lia'} = 20
20
> $ages{'Ron'} = 21
21
> $ages
{Hale => 20, Lia => 24, Ron => 21}
>
```

You can also declare a hash by using curly braces, however additional care
must be taken so as to avoid declaring a block when the intention was a hash.

Similar to `Array`s, Raku also provides us with `%`-sigilled variables 
(also knowm as **associative variables**) with the explicit purpose of storing
only `Associative` data structures such as `Hash`es, `Map`s, and `Pair`s.
The default type for a `%`-sigilled variable is `Hash`. Thus, when assigning
a comma-separated even list of things or a list of `Pair` objects to a
`%`-sigilled variable, we can drop the hash constructor operator `%()`;
we end up with an `Hash` anyway.

For example:

```perl
> my %ages = 'Lia', 24, 'Hale', 20, 'Ron', 21;
{Hale => 20, Lia => 24, Ron => 21}
> %ages = Hale => 20, Lia => 24, Ron => 21
{Hale => 20, Lia => 24, Ron => 21}
> 
```

# Classes

The `class` statement is used to define new types in Raku as shown in the
following example:

```perl
class Point2D {
    # class variable common, to all class instances. Note the use
    # of the `my` declarator, which scopes lexical variables.
    my $.points-total = 0;

    # instance variables, specific to each class instance. Note the use
    # of the `has` keyword, which scopes attributes to instances of a class
    # or role. `has` is implied for `methods`.
    has Rat $.x;
    has Rat $.y;

    # special method called after object construction, 
    # which is useful for checking things and/or modify attributes.
    method TWEAK() {
        $.points-total += 1;
    }

    method translate( Rat $dx, Rat $dy --> Nil ) {
        $!x += $dx;
        $!y += $dy;
    }
}

my $p = Point2D.new(x => 2.0, y => 3.0);
say $p.x;           #=> 2.0
say $p.y;           #=> 3.0

say $p;             #=> (2, 3)
put $p;             #=> Point2D(2, 3)
print $p, "\n";     #=> Point2D(2, 3)
say $p.raku;        #=> Point2D.new(x => 2.0, y => 2.0)"
```

Classes in Raku just like classes in other languages have class variables,
attributes, class methods, and instance methods. Unlike in other programming
languages, there's no special method that must be set up each time for the
initialization of attributes. Any attribute declared with the `.` twigil
can be set up via the `new` constructor. Such an attribute declared this way
is also provided with a read-only method of the same name. The above defined
class can be instantiated by calling the `new` constructor on the class.
By default, the `new` constructor only accepts named parameters which
are named after the attributes declared with the `.` twigil. For example:

```perl
> my $p = Point2D.new(x => 2.0, y => 3.0)
Point2D.new(x => 2.0, y => 3.0)
> $p = Point2D.new(x => Rat, y => Rat)
```

By default, all methods can be called both on the class (i.e., type object)
and on instances on the class (i.e., instance object) depending what they
reference. If a method references something that's only available after the
object is constructed, then calling it on a class will produce an error. 
Raku makes the `self` term inside each method and it refers to the current 
object instance. Methods are called in Raku using the dot notation syntax as
shown below:

```perl
> my $point = Point2D.new(x => 2.0, y => 3.0)
Point2D.new(x => 2.0, y => 3.0)
> $point.x
2
> $point.y
3
> $point.translate(1.0, 1.0)
Nil
>
```

Raku is built on a metaobject layer and thus offers several *metamethods* for
the instropection of objects: 

* `WHAT` for the type object of the type

```perl
> $point.WHAT
(Point2D)
>
```

* `WHICH` for the object's identity value

```perl
> $point.WHICH
Point2D|94616657035376
>
```

* `WHO` for the package supporting the object

```perl
> $point.WHO
Point2D
>
```

* `WHERE` for the memory address of the object

```perl
> $point.WHERE
Point2D|94491003317744
>
```

* `HOW` for the metaclass object

```perl
> $point.HOW
Perl6::Metamodel::ClassHOW.new
>
```

`Metamodel::ClassHOW` is the metaclass behind the `class` keyword.

* `WHY` for the attached Pod value

```perl
> $point.WHY
No documentation available for type 'Point2D'.
Perhaps it can be found at https://docs.raku.org/type/Point2D
>
```

* `DEFINITE` for determining if the object has a valid concrete representation

```perl
> $point.DEFINITE
True
>
```

* `VAR` for the underlying Scalar object, if there is one.

```perl
> $point.VAR
Nil
>
```

# Roles

Roles are another way to structure your Raku code. They're similar to classes 
in that they are a collection of attributes and methods. However, unlike
classes, roles are meant for describing only parts of an object's behavior.
They're are intended to be composed (or *mixed*) into classes and objects. In
general, classes are meant for managing objects and roles are meant for managing
behavior and code reuse within objects.

Roles are declared using the `role` keyword role following by the name of the
role. While classes implements inheritance via the `is` keyword, roles do it
with the `does` keyword.

```perl
role Debug {
    method who-am-i {
        "{self.^name} (#{self.WHERE}): {self.Str}"
    }
}

class Phonograph does Debug {
    has $.name;

    method Str {
        $!name
    }
}

class EightTrack does Debug {
    has $.name;

    method Str {
        $!name
    }
}


my $ph = Phonograph.new(name => "West End Blues");
my $et = EightTrack.new(name => "Surrealistic Pillow");

say $ph.who-am-i; #=> Phonograph (#140571953335944): West End Blues
say $et.who-am-i; #=> EightTrack (#140571953337288): Surrealistic Pillow
```

By including the `Debug` role, both the `Phonograph` and `EightTrack` classes
gain access to the `who-am-i` method.

# Modules

Subroutines, classes, and roles provide the means for structuring your Raku code
but as a codebase grows in size and complexity, there is a need for such
code to be split into multiple files with each source file containing related
definitions. The source files can then be imported as needed in order to access
definitions in any of such source file. In Raku, we refer to source files as
modules; they have the `.rakumod` (for *Raku Module*) extension.

Modules are typically packages (`classes`, `roles`, etc.), subroutines, and
sometimes variables. In Raku, a module can also refer to a type of package
declared with the `module` keyword. To keep the scope of the section within
arm's reach, we use the term "module" to refer to a set of source files in a
namespace.

For example, the `Point2D` class definition from the previous section can
be saved to a module named `Point2D.rakumod`. To use this module elsewhere,
we'd normally utilize the `use` keyword to load and then import it
at compile time. Loading and importing are two processes achieved
by the `need` and `import` keywords respectively; the `use` keyword is
equivalent to `need`ing and `import`ing a module in that order.

For example, we could've a file named `main.raku` with the following contents:

```perl
use v6;

use lib "."; # same directory as main.raku
use Point2D;

my $p = Point2D.new(x => 2.0, y => 3.0);
say $p;
```

Then, we'd execute the `main.raku` as follows:

```text
$ raku main.raku
(2.0, 3.0)
$ 
```

The `Point2D.rakumod` module isn't found in the normal ecosystem so we must tell
our program where to find it with the `lib` pragma. Alternatively, we
could use the environment variable `RAKUDOLIB` to indicate the module's
location, and forgo using the `lib` pragma in the program. For example:

```text
$ RAKUDOLIB=. raku main.raku
(2.0, 3.0)
$ 
```

The environment variable `RAKUDOLIB` is particularly useful when you're
developing a module and you're tweaking and testing it regularly. Raku modules
are usually placed under a `lib` directory so making the module available
to a program `use`-ing is as `RAKUDOLIB=lib raku main.raku`.

# Exceptions

Raku has support for exceptions and exception handling. For example, 
in the following example we try to convert a string to a number and
it results in a `Failure`:

```perl
my $x = +"a";
say $x.^name; #=> Failure
```

In Raku, a `Failure` is a soft or unthrown `Exception`, which acts as a wrapper
around an `Exception` object.

During the execution of a program, an exception is raised when an error occurs;
if the exception is not handled, a trace-back is dumped to the screen. Errors
that are not handled will normally cause an executing program to terminate.
In Raku, sink (void) context causes a `Failure` to throw, i.e. turn into a
regular exception. In our example above, the `Failure` was stored in a variable
and thus no sink context. However, trying to use the variable with the stored
`Failure` will throw it.

```perl
say $x + 2;

# OUTPUT: 
# Cannot convert string to number: base-10 number must begin with valid digits or
# '.' in '⏏a' (indicated by ⏏)
#   in block <unit> at ./main.raku line 2
# 
#   Actually thrown at:
#     in block <unit> at ./main.raku line 6
```

In Raku, exceptions are contained in `try` blocks, which stores them in the `$!`
variable. Any exception that is thrown in such a block will be caught by a `CATCH`
block, either implicit or provided by the user. In the latter case, any
unhandled exception will be rethrown. If you choose not to handle the exception,
they will be contained by the block.

```perl
try {
    my $x = +"a";
    say $x.^name; # This doesn't run
}

if $! {
    say "Something went wrong!"
}             #=> Something failed!
say $!.^name; #=> X::Str::Numeric
```

Exceptions in Raku are diverse. For example, in the previous example 
`+"a"` produced an exception of type `X::Str::Numeric`. We can use `CATCH`
blocks to handle the different types of exceptions. `CATCH` blocks can also
be used in combination with `try` blocks.

{{< hint info >}} 
You're encouraged to read about the different Raku exception types 
at https://docs.raku.org/type-exception.html to learn about the available
exceptions. To get the name of a caught exception,
you can call `.^name` on it similar to how we did in the previous example.
{{< /hint >}}

```perl
try {
    my $x = +"a";
    
    CATCH {
        when X::Str::Numeric {
            say "You tried to convert a string into a number";
        }

        when X::OutOfRange {
            say "Index outside of range"
        }

        default {
            say "Another type of exception"
        }
    }
}
```

Custom exceptions can be defined to handle exceptional circumstances in our
code. To do this, define a custom exception class that inherits from the
`X::Exception` class. For example:

```perl
# taken from 
# https://modules.raku.org/dist/Concurrent::Queue:cpan:JNTHN/lib/Concurrent/Queue.pm6

class X::Concurrent::Queue::Empty is Exception {
    method message() {
        "Cannot dequeue from an empty queue"
    }
}
```

As illustrated by the example above, you might need to override one or more
methods inherited from the `Exception` class depending on how fine-grained
your exception must be. Here, we override the `message` method to provide a
relevant message from our custom exception. You're encouraged to read more about
the `Exception` class at https://docs.raku.org/type/Exception.

# Input and Output

Raku has support for reading and writing to and from input and output sources.
The file on the hard drive is the most popular IO device.

## Reading from files

One way to read the contents of a file is to open the file via the open function
with the :r (read) file mode option and slurp in the contents:

```perl
my $fh = open "file.txt", :r;
my $contents = $fh.slurp;
$fh.close;
```

In the example above, we explicitly close the filehandle using the `close` method
on the `IO::Handle` object returned by the `open` routine. This is a very
traditional way of reading the contents of a file. However, Raku offers 
a cleaner and shorter way of achieving the same result by using the `IO` role
in the filename string and then call the `slurp` routine on the the file
object. 

```perl
my $contents = "file.txt".IO.slurp; # in method call form
$contents    = slurp "testfile"     # same in procedural form
```

We can also read a file line by line by calling/invoking the `lines` 
on the file object. For example:

```perl
for "file.txt".IO.lines -> $line {
    say $line
}
```

## Writing to files

Writing to files in Raku is also easy. When writing to data to a file,
we have the choice of the traditional method of calling the `open` function
(this time with the :w (write) option) and printing the data to the file:

```perl
my $fh = open "file.txt", :w;
$fh.say("Raku rocks!"); # same as $fh.print("Raku rocks!\n");
$fh.close;
```

We can simplify this by using the `spurt` routine to open the file in write
mode, writing the data to the file and closing it again for us:

```perl
"file.txt".spurt("Raku rocks!\n")
```

By default all (text) files are written as UTF-8, however if necessary, an
explicit encoding can be specified via the `:enc` option:

```perl
spurt "afile.txt", "latin1 text: äöüß", enc => "latin1";

# same as:
"afile.txt".spurt("latin1 text: äöüß", enc => "latin1");
```

To append to a file, specify the `:a` option when opening the filehandle
explicitly:

```perl
my $fh = open "file.txt", :a;
$fh.print("Perl also rocks!\n");
$fh.close;
```

This can be simplified even further by using the `:append` option in the call
to `spurt`:

```perl
"file.txt".spurt("Perl also rocks!\n", :append);
```

# Getting Help

The documentation for the Raku programming language can be found at
https://docs.raku.org/.

There's also the module `p6doc` that allows you to to browse the documentation
from your command line. Visit https://github.com/Raku/doc#install-p6doc
to learn about this tool, its installation, and its usage.
