# Classes

The `class` statement is used to define new types in Raku as shown in the
following example:

```perl
class Point2D {
    # class variable common, to all class instances. Note the use
    # of the `my` declarator, which scopes lexical variables.
    my $.points-total = 0;

    # instance variables, specific to each class instance. Note the use
    # of the `has` keyword, which scopes attributes to instances of a class
    # or role. `has` is implied for `methods`.
    has Rat $.x;
    has Rat $.y;

    # special method called after object construction, 
    # which is useful for checking things and/or modify attributes.
    method TWEAK() {
        $.points-total += 1;
    }

    method translate( Rat $dx, Rat $dy ) {
        $!x += $dx;
        $!y += $dy;
    }
    
    # string representation of a Point2D. `Str` is implicitly called by both
    # the `print` and `put` routines.
    method Str {
        "Point2D($!x, $!y)"
    }

    # human readable representation of a Point2D. `gist` is implicitly called
    # by the `say` routine.
    method gist {
        "($!x, $!y)"
    }

    # evaluable representation of the object that can be used via `EVAL` to
    # reconstruct the value of the object.
    method raku {
        "Point2D.new(x => $!x, y => $!y)"
    }
}

my $p = Point2D.new(x => 2.0, y => 3.0);
say $p.x;           #=> 2.0
say $p.y;           #=> 3.0

say $p;             #=> (2, 3)
put $p;             #=> Point2D(2, 3)
print $p, "\n";     #=> Point2D(2, 3)
say $p.raku;        #=> Point2D.new(x => 2.0, y => 2.0)"
```

Classes in Raku just like classes in other languages have class variables,
attributes, class methods, and instance methods. Unlike in other programming
languages, there's no special method that must be set up each time for the
initialization of attributes. Any attribute declared with the `.` twigil
can be set up via the `new` constructor. Such an attribute declared this way
is also provided with a read-only method of the same name. However, if the need
arises, Raku allows you to finely influence the object construction process
right from the moment the arguments are passed to a constructor method up to and
after the object is constructed. 

{{< details title="Sigils and twigils" open=true >}}

So far every variable we've declared had `$`, `@`, or `%` preceding its
identifier (i.e., name). In Raku, these symbols are known as **sigils** and 
they indicate the structural interface for the variable, such as whether it
should be treated as a single value (`$`), a compound value (`@` and `%`),
a subroutine (`&`), etc.

Among some of the reasons why sigils exist in Raku are: ease of variable
interpolation into strings, creation of micro-namespaces for variables and thus
avoiding name clashes, easy single/plural distinction, etc.

Raku doesn't stop there and also provide what are called *secondary sigils* (or
**twigils** for short). Twigils influence the scoping of a variable and like
the name implies they must be used in conjuntion with a *primary sigil*. Some
of them are `!` (attribute scope), `.` (for method method), `*` (for dynamic
scope), `:` (for self-declared formal named parameter), `^`
(for self-declared formal positional parameter), etc. For example:

```
$!a # a scalar variable with attribute scoped
@!b # a positional variable with attribute scoped
$*c # a scalar variable with dynamic scope
$^d # a scalar self-declared positional parameter
```

In this section, we'll use the `!` and `.` twigils. The others will use them
in later chapters. To find out about the remaining twigils, read the 
[Twigils](https://docs.raku.org/language/variables#index-entry-Twigil) in the
documentation.
{{< /details >}}

By default, the `new` constructor only accepts named arguments. However,
you can override the `new` constructor and/or add additional constructors by
using `multi` methods to accept whatever type of arguments and any number of
them. We'll talk more about `multi` in later chapters.

Here we're showing how to instantiate the `Point2D` class:

```perl
> Point2D.new(x => 2.0)
Point2D.new(x => 2.0, y => Rat)
> Point2D.new(y => 3.0)
Point2D.new(x => Rat, y => 3.0)
> Point2D.new(x => 2.0, y => 3.0)
Point2D.new(x => 2.0, y => 3.0)
>
```

## Attributes

Attributes are variables that exist per instance of a class and they're declared
with the `has` keyword. They're declared with the `!` twigil. In Raku all
attributes are *private* and thus innaccesible to anything outside the class
including the `new` constructor. Within the class (i.e., within the class's
methods), they're accessed using the same `!` twigil in conjuntion with 
their respective sigil.

```perl
class Private {
    has $!scalar;
    has @!positional;
    has %!associative;
    
    method set-attributes {
        $!scalar      = "I'm scalar";
        @!positional  = [1, 2, 3];
        %!associative = %(true => 1, false => 0);
    }
}

my $priv = Private.new;
$priv.set-attributes();
```

Instead of the `!` twigil, the `.` twigil can be used in an attribute. When the
latter twigil is used two things happens: besides declaring an attribute with `!`
(behind the scenes), Raku generates an accessor method for the attribute
that connects it to the outside world. This also means that the attribute
can be set up via the `new` constructor at object construction. In this sense,
any attribute declared with `.` twigil is said to be *public*.

```perl
class Public {
    has $.scalar;
    has @.positional;
    has %.associative;
}

my $pub = Public.new(
    scalar      => "I'm scalar".
    positional  => [1, 2, 3],
    associative => %(true => 1, false => 0),
);
```

An attribute declared with the `.` twigil is *somewhat* equivalent to declaring
an attribute with the `!` twigil and providing an accessor method named after
the attribute. For example:

```perl
class A {
    has $!attr;

    method attr {
        return $!attr
    }
}

my $obj = A.new(attr => 5);
say $obj.attr; #=> (Any)
```

Aside from being unable to initialize the attribute `attr` via the `new`
constructor as illustrated by the fact that `$obj.attr` returns `(Any)`,
the above example is similar to the neater example below:

```perl
class B {
    has $.attr;
}

my $obj = B.new(attr => 5);
say $obj.attr; #=> 5
```

{{< details title="Which twigil to use for accessing attributes within the class?" open=true >}}
That depends. What you must keep in mind is that using `!` means a direct access
to the variable while using `.` means that Raku ends up calling the attribute's
generated method on `self`, a term found in other programming languages (e.g.,
Java's `this`, Python's `self`, etc.), and that's available to every method and
bound to the invocant object. Giving that using `.` for attribute access
involves a method call, you should be aware that subclasses may override
those methods and thus change what they may return. If you don't want this to
happen, use the `!` twigil instead.
{{< /details >}}

## Instance and class attributes

Instance attributes are variables specific to an instance object and
scoped to a class via the `has` keyword. We already talked about them
so we'll turn our sight to class attributes. A class attribute is declared
like a regular variable with either the `my` or `our` declarator
to achieve lexical scope or package scope respectively.

```perl
class A {
    my $counter = 0;

    method TWEAK {
        $counter += 1;
    }

    method get-counter {
        $counter
    }
}

# create 5 A objects
A.new for 1..5;

say A.get-counter;
```

The variable `$counter` is lexically scoped and thus only accessible within
the class. However we still created the `get-counter` method to access its
value. We can make use of the `.` twigil in our class attribute so that it
generates an accessor method for us. We can simplify our previous example
as follows:

```perl
class A {
    my $.counter = 0;

    method TWEAK {
        $.counter += 1;
    }
}

# create 5 A objects
A.new for 1..5;

say A.counter;
```


The same example using an `our`-declared variable would look as follows:

```perl
class A {
    our $counter = 0;

    method TWEAK {
        $counter += 1;
    }

    method get-counter {
        $counter
    }
}

# create 5 A objects
A.new for 1..5;

say A.get-counter; #=> 5
say $A::counter;   #=> 5
```

Since `our` variables are package scoped and thus introduce an alias into the
symbol table, they can can be accessed outside the package using
their fully qualified names, as demonstrated in the earlier examples
(e.g., `$A::counter`).

{{< hint warning >}}
Unlike instance attributes, class attributes cannot be declared with a `!`
twigil nor accessed with the `!` when declared with `.` twigil. If declared
with the `.` twigil, they must be accessed with the same twigil as demonstrated
by the examples above.
{{< /hint >}}


## Methods

Methods in Raku are declared with the `method` keyword. Similar to attributes,
they can be declared with `has` but that's implied and it can be omitted.
Methods can be either **public** or **private**. All method declarations we've seen
so far are public. In order to declare a method as private, simply place
an exclamation mark `!` before the method's name. These methods cannot
be called from anywhere outside the defining class. Private methods are invoked
with an exclamation mark instead of a dot.

```perl
class A {
    method !private-method {
        "I'm private"
    }
}
```

## Instance and class methods

By default, a method can either be called on both a class and its instances or
only on its instances, depending what a method references itself. For example,
take the following class `A` that declares a private attribute `$!name` (with
a default value) and a method `get-name` that returns it:

```perl
class A {
    has $!name = 'class A';

    method get-name {
        return $!name
    }
}
```

If we instantiate the class and then call `get-name` on the instance object,
we get the expected result:

```perl
> A.new.get-name
class A
>
```

However, trying to invoke the method directly on the class results in
a runtime error:

```perl
> A.get-name
Cannot look up attributes in a A type object
  in method g at <unknown file> line 1
  in block <unit> at <unknown file> line 1

>
```

The reason for this is that the type itself doesn't have instance attributes
of its own and thus Raku is unable to look up attributes in the type object.
We must remember that `has`-scoped variables (aka instance attributes)
belong to instance of a class, not to the class itself. We could've prevented
that error by catching a lot earlier, i.e., when we try to invoke the method.

By default, we can invoke methods on both classes (i.e., type objects) and
instances of these classes. However, Raku allows to restrict the objects on
which a method can invoked on. If the method can only be invoked on instance
objects, then we're talking about **instance methods**. By the contrary,
if the method can only be invoked on type objects, then we're talking about
**class methods**. Before we get to them, let's talk about `self`.

Raku makes available the `self` keyword to all the methods inside a class,
which is bound to the invocant object. Since `self` is bound to the current
invocant, that's what it's used to make invocations inside the class. However,
`self` isn't anything special and should you wish to have an *explicit invocant*
Raku is there for you. A method's signature can have an explicit invocant as its
first parameter followed by a colon, which allows for the method to refer to
the object it was called on. For example (bear in mind we're using extra
whitespace in the method's signature to better illustrate; they aren't needed):

```perl
class Point3D {
	method who-am-i( $point : ) {
		say "Explicit invocant: ", $point.^name;
        say "Implicit invocant: ",   self.^name;
	}
}
```

Keep in mind that the parameter `$point` is just another alias for `self`
and `$point.^name` and `self.^name` achieves the same thing: calling a
method on the current invocant object. 

```perl
> Point3D.new.who-am-i
Explicit invocant: Point3D
Implicit invocant: Point3D
```

{{< details title="Why do we use a colon for declaring an explicit invocant?" open=true >}}
Remember that a method's signature can have both positional and named parameters,
and thus there was a need to separate the explicit invocant from the regular
parameter list in the method's signature.
{{< /details >}}


Now that we know about explicit invocants, we can talk about instance and class
methods. Declaring either of them boils down to the method's signature. In addition
to allowing for an explict invocant, the method signature also allows for
defining the method as either as a class method, or as an instance method,
through the use of type constraints. The `::?CLASS` variable (think of it as 
single unit and don't get bog down by the colons) can be used to provide the
class name at compile time, combined with either `:U` (for class methods) or
`:D` (for instance methods). Thus, with our previous `A` class:

```perl
class A {
    has $!name = 'class A';

    method get-name( ::?CLASS:D : ) {
        return $!name
    }
}
```

Note that we didn't declare an explicit invoncant, mainly because we didn't
think necessary. The colon is still needed because we must separate
`:?CLASS:D` from the regular parameter list.

Invoking the method on an instance object works as expected:

```perl
> A.new.get-name
class A
```

Invoking the method on a type object fails but with a much more useful message:

```perl
> A.new.get-name
Invocant of method 'get-name' must be an object instance of type 'A', not a type object of type 'A'.  Did you forget a '.new'?
  in method get-name at ./main.raku line 4
  in block <unit> at ./main.raku line 10
 
>
```

{{< hint info >}}
There's another version of the type constrains `:D` and `:U`, and that's `:_`
(for either defined or undefined). `:_` can be used but this is unnecessary
since this is the default constraint on arguments.
{{< /hint >}}

