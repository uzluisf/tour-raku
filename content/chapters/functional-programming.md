---
title: Functional programming
---

The subroutine (or function, procedure, etc) is another construct to organize
Raku code. Raku subroutines are either named or anonymous set of statements 
of expressions. In Raku, **subroutines are first class objects** which means
that there is no restriction on function use as values; introspection on
functions can be carried out, functions can be assigned to variables,
functions can be used as arguments to other function and functions can be
returned from method or function calls just like any other Raku value such
as strings and numbers.

# Subroutine definitions

The `sub` keyword is the usual way of creating user-defined subroutines. 
Subroutines definitions are executable set of statements.

```perl
sub square( $x ) {
	$x * $x
}
```

**When a subroutine definition such as the `square` subroutine defined above
is encountered, only the function definition statement, that is def square(x), is executed; this implies that all arguments are evaluated. The evaluation of arguments has some implications for function default arguments that have mutable data structure as values; this will be covered later on in this chapter. The execution of a function definition binds the function name in the current name-space to a function object which is a wrapper around the executable code for the function. This function object contains a reference to the current global name-space which is the global name-space that is used when the function is called. The function definition does not execute the function body; this gets executed only when the function is called.**

Raku also has support for *lambda expressions* in multiple ways.

# Subroutines are objects

Subroutines 


