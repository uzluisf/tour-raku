---
title: Introduction
---

A *Whirlwind Tour of Raku* is a fast-paced introduction to essential features of
the Raku programming language, aimed at researchers and developers who are
already familiar with programming in another language.
